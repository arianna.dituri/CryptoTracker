//Vendita di un determinato quantitivo di una cryptovaluta, il cui controvalore in EUR o USD ritorna a disposizione dell’utente, nel suo conto principale (EUR o USD)
package CryptoTracker.CryptoTracker;

import java.util.ArrayList;

public class Sell extends Transazione {
	float saldoCrypto;
	float saldoEur;
	float valoreImportoCrypto;
	float guadagnoEur;
	float conversioneCryptoEur;
	Quotazione quotazione;
	ContoCorrente cc=new ContoCorrente();
	
	@Override
	public void transazione(ArrayList<Quotazione> al) {
		
	for(int i=0; i<al.size(); i++) {
		quotazione=al.get(i);
		if(quotazione.getTipoTransazione().equals("Sell")) {
			
			saldoCrypto=quotazione.getConti().getSaldoCrypto();
			valoreImportoCrypto=quotazione.getValoreImportoCrypto();
			saldoEur=cc.getSaldoEur();
			conversioneCryptoEur=quotazione.getConversioneCryptoEur();
			guadagnoEur=valoreImportoCrypto*conversioneCryptoEur;
			
			saldoEur+=guadagnoEur;
			cc.setSaldoEur(saldoEur);
			
			saldoCrypto+=valoreImportoCrypto;
			quotazione.getConti().setSaldoCrypto(saldoCrypto);
			
		}
	}
	}
	

}
