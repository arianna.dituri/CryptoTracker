//Acquisto di un determinato quantitivo di una cryptovaluta, a fronte di un controvalore in EUR o USD (+ una commissione di acquisto)
package CryptoTracker.CryptoTracker;

import java.util.ArrayList;

public class Buy extends Transazione {
	float saldoCrypto;
	float valoreImportoCrypto;
	float balance;
	float eurSpesi;
	float conversioneCryptoEur;
	Quotazione quotazione;
	ContoCorrente cc=new ContoCorrente();
	
	@Override
	public void transazione(ArrayList<Quotazione> al) {

		for(int i=0; i<al.size(); i++) {
			quotazione=al.get(i);
			
			if(quotazione.getTipoTransazione().equals("Buy")) {
				
				saldoCrypto=quotazione.getConti().getSaldoCrypto();
				valoreImportoCrypto=quotazione.getValoreImportoCrypto();
				balance=cc.getSaldoEur();
				conversioneCryptoEur=quotazione.getConversioneCryptoEur();
				eurSpesi=valoreImportoCrypto*conversioneCryptoEur;
				
				balance-=eurSpesi;
				cc.setSaldoEur(balance);
				
				saldoCrypto+=valoreImportoCrypto;
				quotazione.getConti().setSaldoCrypto(saldoCrypto);
				
			}
		}
	}

}
