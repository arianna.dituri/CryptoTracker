package CryptoTracker.CryptoTracker;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

	public static void main(String[] args) {

		ContoCorrente cc=new ContoCorrente();
		Conti cCrypto=new Conti();
		Transazione t=new Transazione();
		
		Utils letturaCsv=new Utils();
		Conti genConti=new Conti();
		
		ArrayList<Quotazione> listaTransazioni=new ArrayList<Quotazione>();
		listaTransazioni=letturaCsv.readFile("Coinbase-TransactionsHistoryReport.csv");
		
		HashMap<String, Float> conti=new HashMap<String, Float>();
		conti=genConti.generaConti("Coinbase-TransactionsHistoryReport.csv");

		 for(int i = 0; i < listaTransazioni.size(); i++) {
	         System.out.println(listaTransazioni.get(i));
	     }
		 
		 System.out.println("\n------------------------------------------\n");
		 
		 for(String k : conti.keySet()) {
			 System.out.println(k + ": " + conti.get(k));
		 }
		 
		 t.transazione(listaTransazioni);
		
	}
}
