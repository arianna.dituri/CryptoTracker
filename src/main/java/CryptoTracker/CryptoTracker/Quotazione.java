package CryptoTracker.CryptoTracker;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Quotazione extends Conti{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Conti conti;
	
	String data;
	String tipoTransazione;
	//String cryptovaluta;
	float valoreImportoCrypto;
	String valutaEur;
	float conversioneCryptoEur;
	float subtotaleSpeso;
	float totaleSpeso;
	float tassa;
	String nota;
	
	
	public Quotazione() {	
	}
	
	public Quotazione(String data, String tipoTransazione, String cryptovaluta, float valoreImportoCrypto, String valutaEur, float conversioneCryptoEur, float subtotaleSpeso, float totaleSpeso, float tassa, String nota) {
		this.data=data;
		this.tipoTransazione=tipoTransazione;
		this.cryptovaluta=cryptovaluta;
		this.valoreImportoCrypto=valoreImportoCrypto;
		this.valutaEur=valutaEur;
		this.conversioneCryptoEur=conversioneCryptoEur;
		this.subtotaleSpeso=subtotaleSpeso;
		this.totaleSpeso=totaleSpeso;
		this.tassa=tassa;
		this.nota=nota;
		conti=new Conti();
	}
	
	public Quotazione(String string) {
		// TODO Auto-generated constructor stub
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getTipoTransazione() {
		return tipoTransazione;
	}

	public void setTipoTransazione(String tipoTransazione) {
		this.tipoTransazione = tipoTransazione;
	}

	public float getValoreImportoCrypto() {
		return valoreImportoCrypto;
	}

	public void setValoreImportoCrypto(float valoreImportoCrypto) {
		this.valoreImportoCrypto = valoreImportoCrypto;
	}

	public String getValutaEur() {
		return valutaEur;
	}

	public void setValutaEur(String valutaEur) {
		this.valutaEur = valutaEur;
	}

	public float getConversioneCryptoEur() {
		return conversioneCryptoEur;
	}

	public void setConversioneCryptoEur(float conversioneCryptoEur) {
		this.conversioneCryptoEur = conversioneCryptoEur;
	}

	public float getSubtotaleSpeso() {
		return subtotaleSpeso;
	}

	public void setSubtotaleSpeso(float subtotaleSpeso) {
		this.subtotaleSpeso = subtotaleSpeso;
	}

	public float getTotaleSpeso() {
		return totaleSpeso;
	}

	public void setTotaleSpeso(float totaleSpeso) {
		this.totaleSpeso = totaleSpeso;
	}

	public float getTassa() {
		return tassa;
	}

	public void setTassa(float tassa) {
		this.tassa = tassa;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}
	
	public Conti getConti() {
		return conti;
	}

	public void setConti(Conti conti) {
		this.conti = conti;
	}

	@Override
	public String toString() {
		return "\nTRANSAZIONE:"+"\nData:"+ this.data + "\nTipo transazione:" + this.tipoTransazione+"\nCryptovaluta:"+ this.cryptovaluta + "\nValore importo crypto:"+ this.valoreImportoCrypto +"\nTipo valuta di conversione:" + this.valutaEur+ "\nConversione EUR/(1)Crypto:" + this.conversioneCryptoEur +"\nSubtotale speso:" + this.subtotaleSpeso + "\nTotale speso:" + this.totaleSpeso +"\nTassa:" + this.tassa +"\nNota:"+ this.nota + "\n\n";
	}
}
