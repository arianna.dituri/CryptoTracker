//Ricezione di un determinato quantitivo di una cryptovaluta dallo stesso Exchange di appartenenza (ad: esempio reward o interessi sulle cryptovalute possedute)
package CryptoTracker.CryptoTracker;

import java.util.ArrayList;

public class CoinbaseEarn extends Transazione {
	Quotazione quotazione=new Quotazione();
	float saldoCrypto;
	float valoreImportoCrypto;
	
	@Override
	public void transazione(ArrayList<Quotazione> al) {
		
		for (int i=0; i<al.size(); i++) {
		if(quotazione.getTipoTransazione().equals("Receive")) {
			saldoCrypto=quotazione.getConti().getSaldoCrypto();
			valoreImportoCrypto=quotazione.valoreImportoCrypto;
		
			quotazione.getConti().setSaldoCrypto(saldoCrypto);}
		}
	}

}
