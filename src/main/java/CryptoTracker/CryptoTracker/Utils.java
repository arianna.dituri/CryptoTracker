package CryptoTracker.CryptoTracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Utils extends Quotazione {
	
	public ArrayList<Quotazione> readFile (String filePath) {
	File f=new File(filePath);
	FileReader fr=null;
	BufferedReader reader=null;
	
	ArrayList<Quotazione> listaTransazioni = new ArrayList<Quotazione>();
	
	//LETTURA FILE transactions.csv e generazione oggetti Quotazione
	try {
		if (f.exists()) {
			fr=new FileReader(f);
			reader=new BufferedReader(fr);
			
			String line="";
			do {
				line= reader.readLine();				
				if (line!=null) {
					String [] risultati=line.split("     ");
					
					for (int i=0; i<risultati.length; i++) {
						
					    if (!risultati[i].contentEquals("Timestamp,Transaction Type,Asset,Quantity Transacted,Spot Price Currency,Spot Price at Transaction,Subtotal,Total (inclusive of fees),Fees,Notes")) {
					    	//System.out.println("\nTransazione: " +risultati[i]);
					    	String[] r=risultati[i].split(",");
					    	
					    		if(r[6].equals("\"\"")) {
					    			listaTransazioni.add(new Quotazione(r[0],r[1], r[2], Float.parseFloat(r[3]), r[4], 0.0F, 0.0F, 0.0F, 0.0F, r[9]));
					    		}else listaTransazioni.add(new Quotazione(r[0],r[1], r[2], Float.parseFloat(r[3]), r[4], Float.parseFloat(r[5]), Float.parseFloat(r[6]), Float.parseFloat(r[7]), Float.parseFloat(r[8]), r[9]));
					    		//System.out.println("["+j+"] "+r[j]);
							}
						}
					}
				}while (line!=null);
		}
	}catch (FileNotFoundException e) {
		System.out.println("File non trovato: " + filePath);
		System.out.println("Errore:  " + e.getMessage());
	}catch (IOException e) {
		System.out.println("Errore in lettura file: " + filePath);	
		System.out.println("Errore: " + e.getMessage());
	}finally {
		try {
			if (reader!=null) {
				reader.close();
			}
			if (fr!=null) {
			fr.close();
			}
		}catch(IOException e) {
			System.out.println("Errore in chiusura file");
		}
	}
	return listaTransazioni;
}
}
	


