package CryptoTracker.CryptoTracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Conti{
	
	String cryptovaluta;
	float saldoCrypto;
	
	public Conti() {
	}
	
	public Conti(String cryptovaluta, float saldoCrypto) {
		this.cryptovaluta=cryptovaluta;
		this.saldoCrypto=saldoCrypto;
	}
	
	public String getCryptovaluta() {
		return cryptovaluta;
	}

	public void setCryptoValuta(String cryptovaluta) {
		this.cryptovaluta = cryptovaluta;
	}

	public float getSaldoCrypto() {
		return saldoCrypto;
	}

	public void setSaldoCrypto(float saldoCrypto) {
		this.saldoCrypto = saldoCrypto;
	}

	public HashMap<String, Float> generaConti (String filePath) {
	File f=new File(filePath);
	FileReader fr=null;
	BufferedReader reader=null;
	
	HashMap<String, Float> hm=new HashMap<String, Float>();
	
	try {
		if (f.exists()) {
			fr=new FileReader(f);
			reader=new BufferedReader(fr);
			
			String line="";
			do {
				line= reader.readLine();				
				if (line!=null) {
					String [] risultati=line.split("     ");
					
					for (int i=0; i<risultati.length; i++) {
						
					    if (!risultati[i].contentEquals("Timestamp,Transaction Type,Asset,Quantity Transacted,Spot Price Currency,Spot Price at Transaction,Subtotal,Total (inclusive of fees),Fees,Notes")) {
					    	//System.out.println("\nTransazione: " +risultati[i]);
					    	String[] r=risultati[i].split(",");
					    	for(i=0; i<r.length; i++) {
					    		cryptovaluta=r[2];
					    		saldoCrypto=Float.parseFloat(r[3]);
					    	    hm.put(cryptovaluta, saldoCrypto);
					    	}
						}
					}
				}
				}while (line!=null);
		}
	}catch (FileNotFoundException e) {
		System.out.println("File non trovato: " + filePath);
		System.out.println("Errore:  " + e.getMessage());
	}catch (IOException e) {
		System.out.println("Errore in lettura file: " + filePath);	
		System.out.println("Errore: " + e.getMessage());
	}finally {
		try {
			if (reader!=null) {
				reader.close();
			}
			if (fr!=null) {
			fr.close();
			}
		}catch(IOException e) {
			System.out.println("Errore in chiusura file");
		}
	}
	return hm;
}
}
